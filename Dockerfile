FROM node:20.12-alpine3.19

ENV NODE_ENV=production

WORKDIR /usr/src/app
COPY .env-exemplo-prod ../.env

RUN --mount=type=bind,source=nodejs/package.json,target=package.json \
    --mount=type=bind,source=nodejs/package-lock.json,target=package-lock.json \
    --mount=type=cache,target=/root/.npm \
    npm ci --include=dev

COPY ./nodejs .
RUN mkdir -p dist
RUN chown 1000:1000 -R .

USER node
EXPOSE 4568

CMD npm run start