import {app, db} from '../app';
import request from 'supertest';

it('should recreate tables', async () => {
    await db.sync({force: true}).then(() => {
        console.log('Tabelas recriadas com sucesso (test)')
    })
});

describe("POST /api/profile", () => {
    test("should create a profile pessoa fisica", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "92893322042",
                nome: "Pessoa Fisica 1",
                celular: "19992374812",
                telefone: "1931287123",
                email: "fisica@teste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect(201)
    });
});

describe("POST /api/profile", () => {
    test("should create a profile pessoa juridica", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "68359143000128",
                nome: "Pessoa Juridica 1",
                celular: "19992374813",
                telefone: "1931287124",
                email: "juridica@teste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect(201)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile wrong email", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "68359143000128",
                nome: "Pessoa Juridica 1",
                celular: "19992374813",
                telefone: "1931287124",
                email: "juridicateste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile wrong cnpj", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "1234",
                nome: "Pessoa Juridica 1",
                celular: "19992374813",
                telefone: "1931287124",
                email: "juridica@teste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile wrong cep", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "68359143000128",
                nome: "Pessoa Juridica 1",
                celular: "19992374813",
                telefone: "1931287124",
                email: "juridica@teste.com",
                cep: "123",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile wrong celular", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "68359143000128",
                nome: "Pessoa Juridica 1",
                celular: "123",
                telefone: "1931287124",
                email: "juridica@teste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile wrong telefone", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                cpf_cnpj: "68359143000128",
                nome: "Pessoa Juridica 1",
                celular: "19992374813",
                telefone: "123",
                email: "juridica@teste.com",
                cep: "12716274",
                logradouro: "Rua Um",
                numero: 123,
                complemento: "",
                cidade: "Campinas",
                bairro: "Bairro",
                estado: "SP"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("POST /api/profile", () => {
    test("should not create a profile", async () => {
        return request(app)
            .post("/api/profile/")
            .send({
                nome: "Perfil teste 2"
            })
            .expect('Content-Type', /json/)
            .expect((res) => {
                expect(res.body.message).toContain("Faltam parametros")
            })
            .expect(400)
    });
});

describe("GET /api/profile/1", () => {
    test("should return first profile", async () => {
        return request(app)
            .get("/api/profile/92893322042")
            .expect('Content-Type', /json/)
            .expect(200)
    });
});

describe("GET /api/profile/0", () => {
    test("should not return profile", async () => {
        return request(app)
            .get("/api/profile/abc123")
            .expect('Content-Type', /json/)
            .expect(404)
    });
});