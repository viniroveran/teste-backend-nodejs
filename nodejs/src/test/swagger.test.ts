import {app} from '../app';
import request from 'supertest';

describe("GET /api/docs/swagger.json", () => {
    test("should return swagger.json", async () => {
        return request(app)
            .get("/api/docs/swagger.json")
            .expect('Content-Type', /json/)
            .expect(200)
    });
});

describe("GET /api/docs/v1/", () => {
    test("should return api docs", async () => {
        return request(app)
            .get("/api/docs/v1/")
            .expect('Content-Type', /html/)
            .expect(200)
    });
});

describe("GET /api/docs/v2/", () => {
    test("should return api docs", async () => {
        return request(app)
            .get("/api/docs/v2/")
            .expect('Content-Type', /html/)
            .expect(200)
    });
});