import {app} from '../app';
import request from 'supertest';

describe("GET /", () => {
    test("should return docs themes", async () => {
        return request(app)
            .get("/")
            .expect('Content-Type', /html/)
            .expect((res) => {
                expect(res.text).toContain("Para acessar a documentação (tema claro), navegue para: /api/docs/v1 <br>" +
                    "Para acessar a documentação (tema escuro), navegue para: /api/docs/v2")
            })
            .expect(200)
    });
});