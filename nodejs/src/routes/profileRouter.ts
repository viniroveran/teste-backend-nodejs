import Router from "express-promise-router";
import customerController from '../controllers/profileController';

const router = Router();

router.get('/:cpf_cnpj',
    // #swagger.tags = ['Perfil']
    customerController.getProfile);
router.post('/',
    // #swagger.tags = ['Perfil']
    customerController.postProfile);

export default router;