import Router from "express-promise-router";
import profileRouter from './profileRouter';
import swaggerRouter from './swagger-ui';

const router = Router();

router.use('/api/profile', profileRouter);
router.use('/api/docs', swaggerRouter);

export default router;