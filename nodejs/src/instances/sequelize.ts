/* istanbul ignore file */
import {Sequelize} from 'sequelize';
import dotenv from 'dotenv';

dotenv.config({path: '/usr/src/.env'});

const host: string = process.env.MYSQL_HOST || "mysqldb";
const db: string = process.env.MYSQL_DATABASE || "wefit";
const username: string = process.env.MYSQL_USER || "wefit";
const password: string = process.env.MYSQL_PASSWORD || "w3f!t";

const sequelize = new Sequelize(db, username, password, {
    host: host,
    dialect: 'mysql',
    port: 3306,
});

export default sequelize;