import {Model, DataTypes} from 'sequelize';
import sequelize from "../instances/sequelize";

class Address extends Model {
    declare cep: number;
    declare logradouro: string;
    declare numero: number;
    declare complemento: string;
    declare cidade: string;
    declare bairro: string;
    declare estado: string;
}

Address.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: true,
            primaryKey: true
        },
        cep: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        logradouro: {
            type: new DataTypes.STRING(128),
            allowNull: false
        },
        numero: {
            type: new DataTypes.INTEGER,
            allowNull: false
        },
        complemento: {
            type: new DataTypes.STRING,
            allowNull: true
        },
        cidade: {
            type: new DataTypes.STRING,
            allowNull: false
        },
        bairro: {
            type: new DataTypes.STRING,
            allowNull: false
        },
        estado: {
            type: new DataTypes.STRING,
            allowNull: false
        },
    },
    {
        sequelize,
        tableName: 'addresses',
        timestamps: true
    }
);

export default Address