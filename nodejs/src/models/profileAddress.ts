import {Model} from 'sequelize';
import sequelize from "../instances/sequelize";
import Profile from "./profile";
import Address from "./address";

class ProfileAddress extends Model {
}

ProfileAddress.init(
    {},
    {
        sequelize,
        tableName: 'profiles_addresses',
        timestamps: false
    }
);

Profile.belongsToMany(Address, { through: ProfileAddress });
Address.belongsToMany(Profile, { through: ProfileAddress });

export default ProfileAddress