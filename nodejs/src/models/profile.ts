import {Model, DataTypes} from 'sequelize';
import sequelize from "../instances/sequelize";

class Profile extends Model {
    declare cpf_cnpj: string;
    declare nome: string;
    declare celular: string;
    declare telefone: string;
    declare email: string;
}

Profile.init(
    {
        cpf_cnpj: {
            type: DataTypes.STRING(14),
            allowNull: false,
            primaryKey: true
        },
        nome: {
            type: new DataTypes.STRING(128),
            allowNull: false
        },
        celular: {
            type: new DataTypes.STRING,
            allowNull: false
        },
        telefone: {
            type: new DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: new DataTypes.STRING,
            allowNull: false
        }
    },
    {
        sequelize,
        tableName: 'profiles',
        timestamps: true
    }
);

export default Profile