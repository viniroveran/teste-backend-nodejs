import {Request, Response} from 'express';
import * as EmailValidator from 'email-validator';
import {cpf, cnpj} from 'cpf-cnpj-validator';
import Profiles from '../models/profile';
import Addresses from '../models/address';
import ProfileAddress from '../models/profileAddress';

async function getProfile(req: Request, res: Response) {
    const cpf_cnpj: string = req.params.cpf_cnpj;
    let hasError: boolean = cpf_cnpj === undefined;
    let foundProfile: any = null;

    if (!hasError) {
        try {
            await Profiles.findByPk(cpf_cnpj).then(async (profile) => {
                if (profile === null) {
                    hasError = true;
                } else {
                    foundProfile = profile.dataValues;
                    await ProfileAddress.findOne({where: {ProfileCpfCnpj: cpf_cnpj}}).then(async (profileAddress) => {
                        /* istanbul ignore next */
                        if (profileAddress === null) {
                            hasError = true;
                        } else {
                            await Addresses.findByPk(profileAddress.dataValues.AddressId).then(async (address) => {
                                /* istanbul ignore next */
                                if (address === null) {
                                    hasError = true;
                                } else {
                                    let addressArray = address.dataValues;
                                    delete addressArray.id;
                                    res.status(200).json({...foundProfile, ...addressArray});
                                }
                            });
                        }
                    });
                }
            });
        } catch (error) {
            /* istanbul ignore next */
            hasError = true;
        }
    }

    if (hasError) {
        res.status(404).json({
            status_code: "404",
            message: "Perfil nao encontrado"
        });
    }
}

async function postProfile(req: Request, res: Response) {
    const cpf_cnpj = req.body.cpf_cnpj
    const nome = req.body.nome
    let celular = req.body.celular
    let telefone = req.body.telefone
    const email = req.body.email
    const cep = req.body.cep
    const logradouro = req.body.logradouro
    const numero = req.body.numero
    const complemento = req.body.complemento
    const cidade = req.body.cidade
    const bairro = req.body.bairro
    const estado = req.body.estado

    if (celular !== undefined) {
        celular = celular.replace(/\D/g, '')
    }
    if (telefone !== undefined) {
        telefone = telefone.replace(/\D/g, '')
    }

    let cpfCnpjValido = (cnpj.isValid(cpf_cnpj) || cpf.isValid(cpf_cnpj));
    let emailValido = EmailValidator.validate(email);

    let hasError = (cpf_cnpj === undefined || nome === undefined || celular === undefined ||
        telefone === undefined || email === undefined || cep === undefined || logradouro === undefined ||
        numero === undefined || complemento === undefined || cidade === undefined || bairro === undefined ||
        estado === undefined || cep.length != 8 || !cpfCnpjValido || !emailValido || celular.length != 11 ||
        telefone.length != 10
    );

    console.log(hasError)
    let createdProfileData: any = null;

    if (!hasError) {
        try {
            await Profiles.create({
                cpf_cnpj: cpf_cnpj,
                nome: nome,
                celular: celular,
                telefone: telefone,
                email: email
            }).then(async (profile) => {
                createdProfileData = profile;
                await Addresses.create({
                    cep: cep,
                    logradouro: logradouro,
                    numero: numero,
                    complemento: complemento,
                    cidade: cidade,
                    bairro: bairro,
                    estado: estado
                }).then(async (address) => {
                    await ProfileAddress.create({
                        ProfileCpfCnpj: createdProfileData.cpf_cnpj,
                        AddressId: address.dataValues.id
                    }).then(async () => {
                        res.status(201).json({
                            status_code: "201",
                            message: "Perfil criado"
                        });
                    });
                });
            });
        } catch (error) {
            /* istanbul ignore next */
            hasError = true;
        }
    }

    if (hasError) {
        res.status(400).json({
            status_code: "400",
            message: "Faltam parametros"
        });
    }
}

export default {
    getProfile,
    postProfile
}