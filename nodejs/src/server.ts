import {app, db} from './app';

const port: number = 4568;

db.authenticate().then(() => {
    console.log('Conectado com sucesso ao banco de dados.')
    if (process.env.NODE_ENV != 'production') {
        db.sync({force: true}).then(() => {
            console.log('Tabelas recriadas com sucesso')
            app.listen(port, () => console.log(`O servidor está escutando na porta ${port}.`));
        })
    } else {
        db.sync().then(() => {
            console.log('Tabelas sincronizadas com sucesso')
            app.listen(port, () => console.log(`O servidor está escutando na porta ${port}.`));
        })
    }
}).catch(error => {
    console.error('Erro ao conectar com o banco de dados: ', error);
})

