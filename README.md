## Teste Backend NodeJS - WeFit

### Objetivo
Criar o endpoint necessário para a tela da seguinte imagem funcionar corretamente. Favor considerar as boas práticas necessárias para esse caso de uso.

![formulario.png](https://gitlab.com/viniroveran/teste-backend-nodejs/-/raw/main/formulario.png?ref_type=heads)

----

### Para configurar as variáveis de ambiente:
    cp .env-exemplo .env
Então, configure o arquivo `.env`

### Para iniciar o servidor (produção):
Modifique as credenciais no arquivo `.env` e depois execute:

    docker compose pull && docker compose up -d
Será iniciada a imagem Docker criada pelo job `publish` da Pipeline definida no arquivo `.gitlab-ci.yml` na porta `4568` e o banco de dados MySQL na porta `3386`.

O ambiente de produção está disponível em: https://testewefit.dumbledore.dev/

### Para executar os testes, execute os seguintes comandos:
    docker compose -f docker-compose-test.yml run --rm nodejs-test
    docker compose -f docker-compose-test.yml down

### Para iniciar o servidor (desenvolvimento), execute os seguintes comandos:
    docker-compose -f docker-compose-dev.yml up -d


No ambiente de desenvolvimento, é iniciado o servidor NodeJS na porta `4568`, MySQL na porta `3386` e PhpMyAdmin na porta `8181`. As configurações estão no arquivo `.env`
